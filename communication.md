---
vim: et ts=4 sts=4 sw=4
title: communication à adresser aux collectivités
keywords: [administratif]
author:
- Thomas Bernard
- Marc Chantreux

---

Nous invitons à une réflexion sur l'avenir de nos modes de vie dans une société que les crises environnementales vont redessiner.
open inovation / inteligence collective

angle informatique vs d'autres angles.
réflexion sur les alternatives
chercher ensemble des solutions
* Proposition d'animation autour du sujet
 * présentation des solutions qu'on entrevoit
 * receuille d'autres pistes de reflexion ou solutions
* en partenariat avec le jardin des sciences
* suite d'évenements
  * projection "la bataiille du libre" + world-café -> provoquer le débat

  * conférence sobriété et convivialité numérique
  * install parties et autres ateliers -> agir
  * produire et


https://inetlab.icube.unistra.fr/index.php/Fichier:Sobriete_et_convivialite_numerique_marc_chantreux.pdf

Organisation / Matériel

Durée de la rencontre:
    * 15 min intro
    * projection 87min
    * pause 15 min
    * world café: 3*30min
    * cloture 20min

World cafe:
* Inscription recommendée à l'avance
* Projection vidéo
* 4 ou 5 Tables pour acceuillir 6 personnes par table (rondes si possibles)
* Espace suffisant pour circuler entre les tables
* Table pour proposer du cafe/the/gateau
* Micro si possible
* Espace d'affichage a prévoir
* Papier grand format (paper board)
* Crayons / Feutres



