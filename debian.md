---
vim: et ts=4 sts=4 sw=4
title: notes sur les installs debian à effectuer pendant les install parties
keywords: [debian, install party, linux, lug ]
author: mch
---

en préambule, je note:

* ce document devrait être cindé: pur debian d'un coté, install party de l'autre
* confronter la vision Patrick (PXE) vs [l'install emabuntu](https://emmabuntus.org/novembre-2020-emmade3-1-03-axee-sur-le-reemploi-pour-tous/) (utilisant [calamares](https://calamares.io/about/)?). surtout pour la partie desktop leger.

# questions ouvertes

* logiciels et stratégies de backup pour enduser
  (avec et sans cloud)
* verifier le fait qu'apt n'as pas un lock suite a un crash

# installer et configurer debian

## configurer apt

la configuration suivante est une proposition de standardisation de notre procédure
d'install de debian pour les install parties. Installé et configuré comme suit,
retrouve les avantages d'ubuntu et autres dériv(é)es mais sans les emmerdes.
comment?

* accepter d'utiliser les non-free à l'install et après rend les choses plus simples
* un meilleur curseur entre stabilité et fraicheur des paquets

donc:

* installer une stable avec [la netinst non-officiel non-free](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/10.4.0+nonfree/)
  (cf. pathof pour le PXE) (prévoir avec un adaptateur USB/ethernet quand driver pas dispo)
* mon /etc/apt/sources.list
  * *avec* non-free et contrib
  * *avec* une source CDN parce que la topologie d'internet n'est pas la même que la topologie
    physique et donc le mirroir "local" n'est en fait pas si local que ça en fonction de
    l'operateur. u-strasbg.fr est par exemple une mauvaise idée hors du réseau Osiris.
    Vu qu'il faut une stratégie générique, le CDN me parait être la meilleure solution.
  * pointe *explicitement* sur une distro (aka *buster*, pas stable) parce que pas de
    mise a jour surprise.
  * virer les deb-src
  * ajouter les backports et les proposed updates
  * *toujours* les security update
  * laisser /etc/apt/sources.list vide et utiliser

installer

        <<. cat > /etc/apt/sources.list.d/main.list
        # all the packages
        # the codename is explicit so we can upgrade during install parties
        deb http://deb.debian.org/debian buster main non-free contrib
        .
        <<. cat > /etc/apt/sources.list.d/security.list
        # dont remove that file if you want your debian to be secure
        deb http://deb.debian.org/debian-security/ buster/updates main non-free contrib
        .
        <<. cat > /etc/apt/sources.list.d/freshmeat.list
        # remove this one if you need battle proof debian
        deb http://deb.debian.org/debian buster-updates main non-free contrib
        deb http://deb.debian.org/debian buster-backports main non-free contrib
        deb http://deb.debian.org/debian buster-proposed-updates main non-free contrib
        .

et c'est pas la peine d'installer trop de trucs:

        <<. cat > /etc/apt/apt.conf.d/80flamekuche
        APT::Install-Recommends "false";
        APT::Install-Suggests "false";
        .

stratégie de mise a jour des listes

* tous les jours pour security
* tous les mois pour le reste
* si paquet a mettre a jour mise a jour dispo:
    * choisir entre mettre a jour maintenant ou au prochain redémarrage
    * faire un update complet avant mise a jour? (tant qu'a faire)

donc dans la cron:

        <<. xargs sudo apt update
            -o Dir::Etc::sourcelist=sources.list.d/security.list
            -o Dir::Etc::sourceparts=-
            -o APT::Get::List-Cleanup=0
        .

sudo apt-get update

## à faire après tasksel

### ssh

si télémaintenance acceptée (en attendant d'intaller got quand ce sera rodé):

* changer le port du serveur ssh (67000 pour l'alsace donc 67022 par exemple).
* faire tourner le port ssh sur localhost si pas d'acces local.
* générer une clef avec l'utilisateur.

### bureau

scenarii

#### le pc est potable et la considération écologique ne compte pas

GNOME

* libreoffice-gnome-integration flingue les accélérateurs du menu. il faut le déinstaller.
* proposer pcmanfm en alternative à nautilus:
  * plus simple à utiliser.
  * navigation au clavier qui fonctionne.
* supprimer totem et installer videolan à la place

#### le pc est veillissant ou la personne veut etre un peu écolo

xfce ou lxde... j'utilise pas donc pas de recommendations.


#### le pc est tres vieux ou la personne veut etre tres écolo

y'a plein de solutions pour tous les gouts, ceux que je connais:

* icewm est très proche de windows95 dans le look&feel.
* afterstep et etoile sont très proche de nextstep (ancetre de l'interface de macosx)
  dans le look&feel.
* blackbox: c'est très intuitif mais ne ressemble a rien d'autre que je connaisse.
* le meilleur rapport poids/puissance que je connaisse est dwm (tiling management
  + tag stack). C'est hyper puissant et super leger mais à ne pas mettre dans les mains
  d'un lambda.
* le window manager par defaut (donc maintenance) de netbsd (donc bon gout) est
  [ctwm](https://www.ctwm.org).

le display manager slim est vraiment joli et simple.

quid du leger sous wayland?
* [velox](https://github.com/michaelforney/velox) est un tiling window manager
* display manager?

# Préparation technique des install parties

* faire des depots locaux? (avec un inotify pour reconstruire le depot)
  ou un depot flamekuche pour les softs homebrew dont le lug
  veut bien assurer la maintenance?
* avoir un debian proxy cache le temps de l'install (et|ou un clone? clamaves?)
* serveur pxe (debian semence) facile d'installaion pour les collectivités
  territoriales?

# Divers

### depot local

dpkg-scanpackages ne suffit plus: il faut générer les fichiers release et autres.
les instructions de [page reprepro du wiki debian](https://wiki.debian.org/DebianRepository/SetupWithReprepro) sont presque justes. il manque les étapes décrites dans
[ce thread](https://lists.debian.org/debian-user-french/2020/05/msg00107.html).

plus:

* gpg --armor --export $EMAIL |sudo apt-key add
* TODO: incron pour lancer reprepro ?

### partager les paquets entre les machines

j'ai déjà un serveur http qui tourne sur kodi.l. pour le transfomer
en cache http:

    install -owww-data -gwww-data -m777 -d /var/www/debian-proxy-cache

    add_repo () sed 's#.*#server {\
        listen 80; listen [::]:80;\
        server_name &;\
        location / { proxy_pass  http://&; }\
    }#'

    <<. add_repo
    deb.debian.org
    packages.microsoft.com
    dist.crystal-lang.org
    deb.nodesource.com
    .

depuis les laptops

    # apres apt ...
    capue () sudo apt -o Acquire::http::Proxy=http://kodi.local $1

sur les postes:

    echo 'Acquire::http::Proxy="http://kodi.local";' >> /etc/apt/apt.conf

# aptitude vs apt

aptitude est *la* principale raison de ma fidélité à debian:
c'est un gestionnaire de paquet puissant qui permet de maintenir
un desktop sur la durée (je ne réinstalle pas: je fais le ménage
de temps en temps et je met à jour un peu avant la sortie
de la nouvelle stable).

apt a l'air d'avoir le vent en poupe. je switcherais si j'arrive
à réaliser simplement les operations suivantes

    aptitude search '~P sip-router'
    aptitude search '~i ~Gprotocol::gopher'


