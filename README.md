# install parties

notes techniques et organisationnelles sur le fonctionnement des install parties.

* installer debian comme [ici](debian.html)
* j'aimerais bien qu'on parle d'oasis et 9front dans les install parties

* applis bureautiques/mail/??? les choix ? les demos ??
* lug faisait signer une décharge... ca a servi?

# Ayez en tête que

## N'imposez pas vos choix de geeks!

Évitons de sortir des sentiers battus pour des raisons qui dépassent les personnes
qui nous accordent leur confiance.

* apt, pas aptitude (ni apt-get, les vieux ...): les utilisateurs n'ont pas besoin
  de la puissance d'aptitude et apt est devenu la nouvelle convention sur les fora web.
* quand la personne n'est pas convaincue et que son PC le permet: GNOME est clairement le DE
  avec lequel j'ai les meilleurs retours (en face de xfce, mate...).
* KDE c'est bien mais pas pour le lambda? certains le disent ...
* Nicolas a dit: deepin ça déchire! (et quand on parle utilisateur)
* quoi qu'il en soit: faire une demo! toujours!

## passer plus de temps avec l'utilisateur

(grace a calamares?)

faire des posters:

* le logiciel libre (les 4 libertés)
* linux, les DE (GNOME, KDE, XFCE, ...), les distributions
* c'est quoi non-free et pourquoi on l'ajoute
* parler de securité et stabilité
  * mises à jour! important!
  * backups
  * refuser d'installer autre chose que les paquets
* promouvoir des services alsaciens
    * synchros et backups via rsync chez arn?
    * Chiffrage des synchros ?
    * nextcloud? (et merge avec onecloud?)
* faire la promo des autres activités de le fede
    * ateliers écologie, pistage, contribution
    * support payant?
* proposer un service de téléassistance?

## divers

* installer GPG, enigmail et configurer $EMAIL ?



