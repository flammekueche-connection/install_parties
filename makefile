.ONESHELL:
all: debian.html README.html
%.html: %.md; pandoc $< -o $@
